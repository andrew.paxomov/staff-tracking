package ru.antara.internal.staff.tracking.core.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(
        creatorVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE
)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationResult {
    @JsonProperty(required = true)
    protected String message;
    @JsonProperty(required = true)
    protected int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static OperationResult createSuccess() {
        OperationResult result = new OperationResult();
        result.setCode(200);
        result.setMessage("Operation success");
        return result;
    }

    public static OperationResult createGenericFailed(String message) {
        OperationResult result = new OperationResult();
        result.setCode(500);
        result.setMessage(message);
        return result;
    }
}
