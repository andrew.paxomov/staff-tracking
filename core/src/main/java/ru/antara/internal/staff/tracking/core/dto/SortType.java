package ru.antara.internal.staff.tracking.core.dto;

public enum SortType {
    ASC, DESC
}
