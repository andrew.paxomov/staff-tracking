package ru.antara.internal.staff.tracking.core.services.base;

import ru.antara.internal.staff.tracking.core.dto.OperationResult;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;

import java.util.UUID;

public interface CrudService<T extends AbstractEntity> extends ListService<T> {
    <E extends T> E add(E entity) throws APIException;
    <E extends T> E update(E entity) throws APIException;
    <E extends T> OperationResult delete(E entity) throws APIException;
    OperationResult delete(UUID key) throws APIException;
}
