package ru.antara.internal.staff.tracking.core.services.base;

import ru.antara.internal.staff.tracking.core.dto.FilterWithSortPagedRequest;
import ru.antara.internal.staff.tracking.core.dto.PagedResponse;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;

import java.util.List;
import java.util.UUID;

public interface ListService<T extends AbstractEntity> {

    PagedResponse<T> list(FilterWithSortPagedRequest pager) throws APIException;

    PagedResponse<T> search(FilterWithSortPagedRequest pager) throws APIException;

    List<T> all() throws APIException;

    <E extends T> E getByID(UUID key) throws APIException;

    long count();

}
