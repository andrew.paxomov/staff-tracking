package ru.antara.internal.staff.tracking.core.dto;

public enum FilterType {
    EQUAL, LIKE, BETWEEN, NOT_EQUAL, IS_NULL, IS_NOT_NULL, GREATER, LESS
}
