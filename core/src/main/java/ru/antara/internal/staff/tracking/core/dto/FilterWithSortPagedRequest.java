package ru.antara.internal.staff.tracking.core.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonAutoDetect(
        creatorVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE
)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilterWithSortPagedRequest extends PagedRequest {

    @JsonProperty
    private Map<String, SortType> sort;

    @JsonProperty
    private Map<String, FilterItem> filters;

    public Map<String, SortType> getSort() {
        return sort;
    }

    public void setSort(Map<String, SortType> sort) {
        this.sort = sort;
    }

    public Map<String, FilterItem> getFilters() {
        if (filters == null) {
            this.filters = new HashMap<>();
        }
        return filters;
    }

    public void setFilters(Map<String, FilterItem> filters) {
        this.filters = filters;
    }

    public static FilterWithSortPagedRequest createDefault() {
        FilterWithSortPagedRequest pagedRequest = new FilterWithSortPagedRequest();
        pagedRequest.setPage(1);
        pagedRequest.setPerPage(20);
        pagedRequest.setSort(new HashMap<>());
        pagedRequest.setFilters(new HashMap<>());
        return pagedRequest;
    }
}
