package ru.antara.internal.staff.tracking.core.services;

import ru.antara.internal.staff.tracking.core.model.Staff;
import ru.antara.internal.staff.tracking.core.services.base.CrudService;

public interface StaffService extends CrudService<Staff> {
}
