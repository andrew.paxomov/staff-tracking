package ru.antara.internal.staff.tracking.core.dto;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PagedResponse<T extends AbstractEntity> {

    private List<T> items;

    private Long total;

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public static <T extends AbstractEntity> PagedResponse<T> createFrom(List<T> data, Long total) {
        PagedResponse<T> response = new PagedResponse<>();
        response.items = data;
        response.total = total;
        return response;
    }

    public static <T extends AbstractEntity> PagedResponse<T> createFrom(Page<T> data) {
        PagedResponse<T> response = new PagedResponse<>();
        response.items = data.get().collect(Collectors.toList());
        response.total = data.getTotalElements();
        return response;
    }

    public static <T extends AbstractEntity> PagedResponse<T> createEmpty() {
        PagedResponse<T> response = new PagedResponse<>();
        response.items = new ArrayList<>();
        response.total = 0L;
        return response;
    }
}
