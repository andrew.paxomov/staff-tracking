package ru.antara.internal.staff.tracking.core.exceptions;

import java.time.LocalDateTime;

public class APIException extends Throwable {

    private int code;
    private String status;
    private LocalDateTime timestamp;
    private Integer applicationCode;

    public APIException(String message) {
        super(message);
    }

    public APIException(String message, Integer applicationCode) {
        super(message);
        this.applicationCode = applicationCode;
    }

    public APIException(String message, Throwable reason) {
        super(message, reason);
    }

    public APIException(String message, Integer applicationCode, Throwable reason) {
        super(message, reason);
        this.applicationCode = applicationCode;
    }


    public Integer getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(Integer applicationCode) {
        this.applicationCode = applicationCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public static APIException notSupported() {
        return new APIException("Not supported", 1000);
    }

    public static APIException dataAlreadyExist(String message) {
        return new APIException(message, 2000);
    }

    public static APIException dataAlreadyExist() {
        return new APIException("Данные уже есть", 300);
    }

    public static APIException permissionDenied() {
        return new APIException("Permission denied", 403);
    }

    public static APIException unsupportedAccountState() {
        return new APIException("Unsupported account state", 403);
    }

    public static APIException notAllDataPresent() {
        return new APIException("Not all data present", 403);
    }

    public static APIException dataNotFound() {
        return new APIException("Данные не найдены", 403);
    }

    public static APIException dataNotFound(String message) {
        return new APIException(String.format("%s not found", message), 404);
    }

    public static APIException internalServerError(String message) {
        return new APIException(message, 500);
    }
}
