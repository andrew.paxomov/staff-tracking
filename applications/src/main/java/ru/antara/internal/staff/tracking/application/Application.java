package ru.antara.internal.staff.tracking.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

@EnableConfigurationProperties
@SpringBootApplication(
        scanBasePackages={"ru.antara.internal.staff"},
        exclude = {ErrorMvcAutoConfiguration.class, FreeMarkerAutoConfiguration.class}
)
@EntityScan(basePackages={"ru.antara.internal.staff.tracking.core.model"})
@EnableJpaRepositories("ru.antara.internal.staff.tracking")
@EnableAsync
@EnableScheduling
public class Application {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).run(args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            if(Arrays.stream(args).filter(x->x.equalsIgnoreCase("--dump")).findFirst().orElse(null)!=null){
                System.out.println("Let's inspect the beans provided by Spring Boot:");

                String[] beanNames = ctx.getBeanDefinitionNames();
                Arrays.sort(beanNames);
                for (String beanName : beanNames) {
                    System.out.println(beanName+"->");
                    System.out.println("\t"+ctx.getType(beanName));
                }
            }
        };
    }
}
