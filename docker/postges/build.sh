#!/bin/bash
echo "Create image..."
NAME="registry.gitlab.com/antara-services.ru/images/postgres-staff"
IMG="$NAME:1.0.0"
LATEST="$NAME:latest"
docker build -t $IMG .
docker tag $IMG $LATEST
