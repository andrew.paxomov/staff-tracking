#!/bin/bash

if [ ! -n "$GITLAB_USER" ]; then
  echo "Please setup GITLAB_USER env to proper value"
  exit
fi

if [ ! -n "$GITLAB_REGISTRY_TOKEN" ]; then
  echo "Please setup GITLAB_REGISTRY_TOKEN env to proper value"
  exit
fi

echo "Start docker..."

docker login registry.gitlab.com/antara-services.ru/images -u "$GITLAB_USER" -p "$GITLAB_REGISTRY_TOKEN"

echo "Launch composer..."
MODE="$1"
shift
DETOUCH=1
for arg in "$@"
do
  case $arg in
    -nd|--no-detouch)
    DETOUCH=0
    shift
    ;;
  esac
done

docker_up() {
  DOCKER_COMMAND=(docker-compose -f docker-compose.$1.yml -p peoplepass up)
  if [ $2 -eq 1 ]; then
    DOCKER_COMMAND+=(-d)
  fi
  "${DOCKER_COMMAND[@]}"
}

case $MODE in
  *)
    echo "Start docker in production mode..."
    docker-compose -f docker-compose.yml -p antara up -d --force-recreate
  ;;
esac
