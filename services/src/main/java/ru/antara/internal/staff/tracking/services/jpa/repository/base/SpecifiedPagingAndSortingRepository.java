package ru.antara.internal.staff.tracking.services.jpa.repository.base;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;

import java.util.UUID;

@NoRepositoryBean
public interface SpecifiedPagingAndSortingRepository<T extends AbstractEntity, K extends UUID> extends JpaRepository<T, K>, JpaSpecificationExecutor<T> {
    default long count() {
        return this.count(nonDeleted());
    }

    default Specification<T> nonDeleted() {
        return (entity, cq, cb) -> cb.equal(entity.get("deleted"), false);
    }
}
