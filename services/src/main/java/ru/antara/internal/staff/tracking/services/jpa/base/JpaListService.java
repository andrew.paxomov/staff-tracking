package ru.antara.internal.staff.tracking.services.jpa.base;

import org.hibernate.query.criteria.internal.path.PluralAttributePath;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import ru.antara.internal.staff.tracking.core.dto.*;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;
import ru.antara.internal.staff.tracking.core.services.base.ListService;
import ru.antara.internal.staff.tracking.services.jpa.repository.base.SpecifiedPagingAndSortingRepository;

import javax.persistence.criteria.*;
import javax.validation.constraints.NotNull;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class JpaListService<T extends AbstractEntity, R extends SpecifiedPagingAndSortingRepository<T, UUID>> implements ListService<T> {

    protected Class<T> persistentClass;

    protected R repository;

    public R getRepository() {
        return this.repository;
    }

    protected JpaListService(R repository) {
        this.repository = repository;
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public T getByID(UUID key) throws APIException {
        return this.repository.findById(key).orElse(null);
    }

    @Override
    public PagedResponse<T> list(FilterWithSortPagedRequest pager) throws APIException {
        return this.list(filterSpecification(pager.getFilters()).and(notDeletedSpecification()), pager);
    }

    @Override
    public List<T> all() throws APIException {
        return this.all(null);
    }

    @Override
    public long count() {
        return this.repository.count(notDeletedSpecification());
    }

    public PagedResponse<T> search(FilterWithSortPagedRequest pager) throws APIException {
        return this.search(searchSpecification(pager.getFilters()).and(notDeletedSpecification()), pager);
    }

    protected PagedResponse<T> search(@Nullable Specification<T> spec, FilterWithSortPagedRequest pager) throws APIException {
        return PagedResponse.createFrom(this.repository.findAll(notDeletedSpecification().and(searchSpecification(pager.getFilters())).and(spec), createPageable(pager)));
    }

    protected List<T> all(@Nullable Specification<T> spec) {
        return this.repository.findAll(notDeletedSpecification().and(spec), Sort.by("id").ascending());
    }

    protected PagedResponse<T> list(@Nullable Specification<T> spec, FilterWithSortPagedRequest pager) throws APIException {
        return PagedResponse.createFrom(getPagedList(spec, pager));
    }

    protected Page<T> getPagedList(@Nullable Specification<T> spec, FilterWithSortPagedRequest pager) {
        return this.repository.findAll(notDeletedSpecification().and(filterSpecification(pager.getFilters())).and(spec), createPageable(pager));
    }

    protected Pageable createPageable(FilterWithSortPagedRequest pager) {
        Sort sort = buildSort(pager);
        return sort != null ? PageRequest.of(pager.getPage() - 1, pager.getPerPage(), sort) : PageRequest.of(pager.getPage() - 1, pager.getPerPage());
    }

    protected Sort buildSort(FilterWithSortPagedRequest pager) {
        AtomicReference<Sort> sort = new AtomicReference<>();
        Optional.ofNullable(pager.getSort()).filter(sortingMap -> sortingMap.size() > 0)
                .ifPresent(sortingMap -> sortingMap.keySet()
                        .stream()
                        .map(key -> getOrderedSorting(sort.get(), sortingMap, key))
                        .forEach(sort::set));
        return sort.get();
    }

    protected Sort getOrderedSorting(Sort sort, Map<String, SortType> sortingMap, String key) {
        return Optional.ofNullable(sort).map(st -> st.and(getOrder(sortingMap, key))).orElseGet(() -> getOrder(sortingMap, key));
    }

    protected Sort getOrder(Map<String, SortType> sortingMap, String key) {
        return SortType.ASC.equals(sortingMap.get(key)) ? Sort.by(key).ascending() : Sort.by(key).descending();
    }

    protected <S extends T> @NotNull Specification<S> notDeletedSpecification() {
        return (entity, cq, cb) -> cb.equal(entity.get("deleted"), false);
    }

    protected <S extends T> Specification<S> filterSpecification(Map<String, FilterItem> filter) {
        return (entity, cq, cb) -> cb.and(createFilterPredicates(entity, cq, cb, filter).toArray(new Predicate[]{}));
    }

    protected <S extends T> Specification<S> searchSpecification(Map<String, FilterItem> filter) {
        return (entity, cq, cb) -> cb.or(createFilterPredicates(entity, cq, cb, filter).toArray(new Predicate[]{}));
    }

    protected <S extends T> List<Predicate> createFilterPredicates(Root<S> entity, CriteriaQuery<?> cq, CriteriaBuilder cb, Map<String, FilterItem> filter) {
        List<Predicate> predicates = new ArrayList<>();
        if (filter != null) {
            for (String k : filter.keySet()) {
                FilterItem item = filter.get(k);
                Predicate predicate = this.constructPredicateFromPath(k, item.getType(), entity, cq, cb, item.getValue());
                if (predicate != null) {
                    predicates.add(predicate);
                }
            }
        }
        return predicates;
    }

    private <S extends T> Predicate constructPredicateFromPath(String k, FilterType type, Root<S> entity, CriteriaQuery<?> cq, CriteriaBuilder cb, Object value) {
        Path path = entity;
        if (k.contains(".")) {
            String[] keys = k.split("\\.");
            cq.distinct(true);
            Join bufJoin = null;
            for (String key : keys) {
                Path<?> bufPath = path.get(key);
                if (bufPath instanceof PluralAttributePath) {
                    bufJoin = bufJoin == null ? entity.join(key) : bufJoin.join(key);
                    path = bufJoin;
                } else {
                    path = bufPath;
                }
            }
        } else {
            path = entity.get(k);
        }
        if (value == null) {
            switch (type) {
                case IS_NULL:
                    if (Collection.class.isAssignableFrom(path.getJavaType()))
                        return cb.isEmpty(path);
                    return cb.isNull(path);
                case IS_NOT_NULL:
                    if (Collection.class.isAssignableFrom(path.getJavaType()))
                        return cb.isNotEmpty(path);
                    return cb.isNotNull(path);
            }
        } else if (Enum.class.isAssignableFrom(path.getJavaType()) && !Collection.class.isAssignableFrom(value.getClass())) {
            Enum enumValue = Enum.valueOf((Class<Enum>) path.getJavaType(), value.toString());
            switch (type) {
                case EQUAL:
                case LIKE:
                    return cb.equal(path, enumValue);
                case NOT_EQUAL:
                    return cb.notEqual(path, enumValue);
            }
            return cb.notEqual(path, enumValue);
        } else if (Collection.class.isAssignableFrom(path.getJavaType()) || Collection.class.isAssignableFrom(value.getClass())) {
            switch (type) {
                case LIKE: {
                    if (Enum.class.isAssignableFrom(path.getJavaType())) {
                        final Class<Enum> valueType = path.getJavaType();
                        Object valuesForNotIn = ((Collection<Object>) value).stream().map(v -> Enum.valueOf(valueType, v.toString())).collect(Collectors.toList());
                        return path.in(valuesForNotIn);
                    } else {
                        return path.in(value);
                    }
                }
                case BETWEEN: {
                    List<String> list = (ArrayList) value;
                    int size = list.size();
                    if (size % 2 == 0) {
                        List<Predicate> predicates = new ArrayList<>();
                        for (int i = 0; i < size; i += 2) {
                            if (LocalDateTime.class.isAssignableFrom(path.getJavaType())) {
                                predicates.add(cb.between(path, LocalDateTime.parse(list.get(i)), LocalDateTime.parse(list.get(i + 1))));
                            } else {
                                predicates.add(cb.between(path, LocalDate.parse(list.get(i)), LocalDate.parse(list.get(i + 1))));
                            }
                        }
                        return cb.and(predicates.toArray(new Predicate[]{}));
                    }
                    return null;
                }
                case NOT_EQUAL: {
                    if (Enum.class.isAssignableFrom(path.getJavaType())) {
                        final Class<Enum> valueType = path.getJavaType();
                        Object valuesForNotIn = ((Collection<Object>) value).stream().map(v -> Enum.valueOf(valueType, v.toString())).collect(Collectors.toList());
                        return cb.not(path.in(valuesForNotIn));
                    } else {
                        return cb.not(path.in(value));
                    }
                }
            }
        } else if (AbstractEntity.class.isAssignableFrom(path.getJavaType())) {
            switch (type) {
                case EQUAL:
                case LIKE:
                    return cb.equal(path.get("id"), UUID.fromString(value.toString()));
                case NOT_EQUAL:
                    return cb.notEqual(path.get("id"), UUID.fromString(value.toString()));
            }
        } else {
            switch (type) {
                case EQUAL:
                    if (LocalDate.class.isAssignableFrom(path.getJavaType())) {
                        return cb.equal(path, LocalDate.parse(value.toString()));
                    }
                    if (LocalDateTime.class.isAssignableFrom(path.getJavaType())) {
                        return cb.equal(path, LocalDateTime.parse(value.toString()));
                    }
                    if (UUID.class.isAssignableFrom(path.getJavaType())) {
                        return cb.equal(path, UUID.fromString(value.toString()));
                    }
                    return cb.equal(path, value);
                case NOT_EQUAL:
                    return cb.notEqual(path, value);
                case LIKE:
                    if (String.class.isAssignableFrom(path.getJavaType())) {
                        return cb.like(cb.upper(path), "%" + value.toString().toUpperCase() + "%");
                    }
                case GREATER:
                    if (LocalDate.class.isAssignableFrom(path.getJavaType())) {
                        return cb.greaterThan(path, LocalDate.parse(value.toString()));
                    }
                    if (LocalDateTime.class.isAssignableFrom(path.getJavaType())) {
                        return cb.greaterThan(path, LocalDateTime.parse(value.toString()));
                    }
                case LESS:
                    if (LocalDate.class.isAssignableFrom(path.getJavaType())) {
                        return cb.lessThan(path, LocalDate.parse(value.toString()));
                    }
                    if (LocalDateTime.class.isAssignableFrom(path.getJavaType())) {
                        return cb.lessThan(path, LocalDateTime.parse(value.toString()));
                    }
            }
        }
        return null;
    }
}
