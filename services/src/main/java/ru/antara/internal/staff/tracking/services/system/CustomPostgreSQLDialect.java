package ru.antara.internal.staff.tracking.services.system;

import org.hibernate.dialect.PostgreSQL95Dialect;

import java.sql.Types;

public class CustomPostgreSQLDialect extends PostgreSQL95Dialect {

    public CustomPostgreSQLDialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
        this.registerHibernateType(1111,"pg-uuid");
    }
}
