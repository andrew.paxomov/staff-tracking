package ru.antara.internal.staff.tracking.services.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.antara.internal.staff.tracking.core.model.Staff;
import ru.antara.internal.staff.tracking.services.jpa.repository.base.SpecifiedPagingAndSortingRepository;

import java.util.UUID;

public interface StaffRepository extends SpecifiedPagingAndSortingRepository<Staff, UUID>, JpaSpecificationExecutor<Staff> {
}
