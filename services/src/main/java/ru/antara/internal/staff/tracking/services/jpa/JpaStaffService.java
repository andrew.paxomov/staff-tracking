package ru.antara.internal.staff.tracking.services.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.antara.internal.staff.tracking.core.model.Staff;
import ru.antara.internal.staff.tracking.core.services.StaffService;
import ru.antara.internal.staff.tracking.services.jpa.base.JpaCrudService;
import ru.antara.internal.staff.tracking.services.jpa.repository.StaffRepository;

@Service
@Primary
public class JpaStaffService extends JpaCrudService<Staff, StaffRepository> implements StaffService {

    protected JpaStaffService(StaffRepository repository) {
        super(repository);
    }
}
