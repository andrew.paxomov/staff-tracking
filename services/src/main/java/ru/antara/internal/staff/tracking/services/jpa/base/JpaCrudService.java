package ru.antara.internal.staff.tracking.services.jpa.base;

import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ru.antara.internal.staff.tracking.core.dto.OperationResult;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;
import ru.antara.internal.staff.tracking.core.services.base.CrudService;
import ru.antara.internal.staff.tracking.services.jpa.repository.base.SpecifiedPagingAndSortingRepository;

import java.util.UUID;

@EnableAspectJAutoProxy(exposeProxy = true)
public class JpaCrudService<T extends AbstractEntity, R extends SpecifiedPagingAndSortingRepository<T, UUID>> extends JpaListService<T, R> implements CrudService<T> {

    protected JpaCrudService(R repository) {
        super(repository);
    }

    @Override
    public <E extends T> E add(E entity) throws APIException {
        return this.repository.saveAndFlush(entity);
    }

    @Override
    public <E extends T> E update(E entity) throws APIException {
        return this.repository.saveAndFlush(entity);
    }

    @Override
    public <E extends T> OperationResult delete(E entity) throws APIException {
        entity.setDeleted(true);
        this.update(entity);
        return OperationResult.createSuccess();
    }

    @Override
    public OperationResult delete(UUID key) throws APIException {
        return this.delete(this.getByID(key));
    }

}
