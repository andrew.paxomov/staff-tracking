package ru.antara.internal.staff.tracking.web.controllers.base;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.antara.internal.staff.tracking.core.dto.OperationResult;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;
import ru.antara.internal.staff.tracking.core.services.base.CrudService;

import java.util.UUID;

public abstract class CrudController<T extends AbstractEntity, S extends CrudService<T>> extends ListController<T, S> implements CrudService<T> {

    public CrudController(S listService) {
        super(listService);
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    @Override
    public <E extends T> E add(@RequestBody E entity) throws APIException {
        return this.service.add(entity);
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    @Override
    public <E extends T> E update(@RequestBody E entity) throws APIException {
        return this.service.update(entity);
    }

    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    @Override
    public <E extends T> OperationResult delete(@RequestBody E entity) throws APIException {
        return this.service.delete(entity);
    }

    @RequestMapping(path = "/{id}/delete", method = RequestMethod.GET)
    @Override
    public OperationResult delete(@PathVariable(name = "id") UUID key) throws APIException {
        return this.service.delete(key);
    }

}
