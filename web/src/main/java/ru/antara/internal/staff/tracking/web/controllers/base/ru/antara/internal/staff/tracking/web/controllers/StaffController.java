package ru.antara.internal.staff.tracking.web.controllers.base.ru.antara.internal.staff.tracking.web.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.antara.internal.staff.tracking.core.model.Staff;
import ru.antara.internal.staff.tracking.core.services.StaffService;
import ru.antara.internal.staff.tracking.web.controllers.base.CrudController;

@RestController
@RequestMapping("/staff")
public class StaffController extends CrudController<Staff, StaffService> implements StaffService {

    public StaffController(StaffService listService) {
        super(listService);
    }
}
