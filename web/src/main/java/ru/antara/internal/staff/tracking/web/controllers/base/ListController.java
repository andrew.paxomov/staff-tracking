package ru.antara.internal.staff.tracking.web.controllers.base;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.antara.internal.staff.tracking.core.dto.FilterWithSortPagedRequest;
import ru.antara.internal.staff.tracking.core.dto.PagedResponse;
import ru.antara.internal.staff.tracking.core.exceptions.APIException;
import ru.antara.internal.staff.tracking.core.model.base.AbstractEntity;
import ru.antara.internal.staff.tracking.core.services.base.ListService;

import java.util.List;
import java.util.UUID;

public abstract class ListController<T extends AbstractEntity, S extends ListService<T>> implements ListService<T> {

    protected S service;

    public S getService() {
        return this.service;
    }

    public ListController(S listService) {
        this.service = listService;
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public PagedResponse<T> list(@RequestBody FilterWithSortPagedRequest pager) throws APIException {
        return this.service.list(pager);
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    @Override
    public PagedResponse<T> search(@RequestBody FilterWithSortPagedRequest pager) throws APIException {
        return this.service.search(pager);
    }

    @RequestMapping(method = RequestMethod.GET)
    @Override
    public List<T> all() throws APIException {
        return this.service.all();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @Override
    public T getByID(@PathVariable(name = "id") UUID key) throws APIException {
        return this.service.getByID(key);
    }

    @Override
    public long count() {
        return this.service.count();
    }

}
